﻿// See https://aka.ms/new-console-template for more information

var maxValue = 12;
var partLength = 6;

// Элементу в позиции i соответствует количество различных комбинаций букв длины partLength с суммой равной i
var counts = new ulong[maxValue * partLength + 1];

// Комбинация букв длины partLength
var letters = new byte[partLength];

CalculateCounts(0, letters, counts);

ulong totalCount = 0;

foreach (ulong count in counts)
    totalCount += count * count * (ulong)(maxValue + 1);

Console.WriteLine(totalCount);

Console.ReadKey();

void CalculateCounts(byte currentLetterIndex, byte[] letters, ulong[] counts)
{
    for (byte i = 0; i <= maxValue; i++)
    {
        letters[currentLetterIndex] = i;
       
        if (currentLetterIndex < letters.Length - 1)
            CalculateCounts((byte)(currentLetterIndex + 1), letters, counts);
        else
        {
            var sum = letters.Sum(l => l);
            counts[sum] = counts[sum] + 1;
        }
    }
}
